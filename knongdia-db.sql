CREATE TABLE kd_role(
	id SERIAL PRIMARY KEY,
	role_name VARCHAR(20) NOT NULL,
	status BOOLEAN NOT NULL DEFAULT TRUE,
	created_date TIMESTAMP DEFAULT now(),
	UNIQUE (role_name)
);

CREATE TABLE kd_user(
	id SERIAL PRIMARY KEY,
	user_name VARCHAR(100) NOT NULL,
	email VARCHAR(100) NOT NULL,
	password VARCHAR(64) NOT NULL,
	role_id INT NOT NULL REFERENCES kd_role(id),
	status BOOLEAN NOT NULL DEFAULT TRUE,
	created_date TIMESTAMP NOT NULL DEFAULT now(),
	UNIQUE(user_name),
	UNIQUE(email)
);

CREATE TABLE kd_category(
	id SERIAL PRIMARY KEY,
	main_cate_id INT REFERENCES kd_category(id) DEFAULT NULL,
	cate_name VARCHAR(50) NOT NULL,
	icon_name VARCHAR (100) DEFAULT 'NO_CATE_IMAGE.jpg',
	description VARCHAR(300),
	status BOOLEAN NOT NULL DEFAULT TRUE,
	created_date TIMESTAMP NOT NULL DEFAULT now(),
	UNIQUE(cate_name)
);

CREATE TABLE kd_cate_keyword(
	id SERIAL PRIMARY KEY,
	cate_id INT NOT NULL REFERENCES kd_category(id),
	keyword_name VARCHAR(40) NOT NULL,
	status BOOLEAN NOT NULL DEFAULT TRUE,
	created_date TIMESTAMP NOT NULL DEFAULT now()
);

CREATE TABLE kd_url(
	id SERIAL PRIMARY KEY,
	title VARCHAR(255) NOT NULL,
	cate_id INT REFERENCES kd_category(id),
	url_type CHAR(1) NOT NULL,
	user_id INT REFERENCES kd_user(id),
	link VARCHAR(100) NOT NULL,
	address VARCHAR(255),
	phone VARCHAR(100),
	email VARCHAR(100),
	description TEXT,
	pic_url VARCHAR(255),
	status BOOLEAN NOT NULL DEFAULT TRUE,
	is_approved BOOLEAN NOT NULL DEFAULT FALSE,
	view INT NOT NULL DEFAULT 0,
	created_date TIMESTAMP NOT NULL DEFAULT now(),
	UNIQUE(link)
);

CREATE TABLE kd_keyword(
	id SERIAL PRIMARY KEY,
	url_id INT REFERENCES kd_url(id) NOT NULL,
	keyword_name VARCHAR(40) NOT NULL,
	status BOOLEAN NOT NULL DEFAULT TRUE,
	created_date TIMESTAMP NOT NULL DEFAULT now()
);

CREATE TABLE kd_new_keyword(
	id SERIAL PRIMARY KEY,
	keyword_name TEXT NOT NULL,
	status BOOLEAN NOT NULL DEFAULT TRUE,
	created_date TIMESTAMP NOT NULL DEFAULT now()	
);


-- insert some data

INSERT INTO kd_role(role_name) VALUES ('ADMIN');

INSERT INTO kd_role(role_name) VALUES ('USER');

INSERT INTO kd_user(user_name, email, password, role_id)
  VALUES ('admin', 'admin@gmail.com', '$2a$06$gZnheE2DdkJRUfKbXTEcleJ1fctGwzrSid/DNDATAiL0i1HeWN3bK', 1);

INSERT INTO kd_user(user_name, email, password, role_id)
  VALUES ('user', 'user@gmail.com', '$2a$06$gZnheE2DdkJRUfKbXTEcleJ1fctGwzrSid/DNDATAiL0i1HeWN3bK', 2);






INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    NULL, 'Education', 'education description', 'book.png'
);

INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    1, 'University', 'University description', 'book.png'
);

INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    1, 'Institute', 'Insif description', 'book.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    1, 'High_school', 'High school description', 'book.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    1, 'Secondary_school', 'Secondary school description', 'book.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    1, 'Primary_school', 'Primary school description', 'book.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    1, 'Kindergarden', 'Kindergarten description', 'book.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    1, 'Center', 'Center description', 'book.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    1, 'E-Learning', 'E-Learning description', 'book.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    1, 'Training', 'Training description', 'book.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    NULL , 'Entertainment', 'Entertainment description', 'movie.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    11, 'Video', 'Video description', 'movie.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    11, 'TV', 'TV description', 'movie.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    11, 'Music', 'Music description', 'movie.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    11, 'Cinema', 'Cinema description', 'movie.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    11, 'Reading', 'Reading description', 'movie.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    11, 'KTV', 'KTV description', 'movie.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    11, 'Game', 'Game description', 'movie.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    NULL , 'News and Media', 'News and Media description', 'newspaper.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    19 , 'Sport News', 'Sport News description', 'newspaper.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    19 , 'Political', 'Political description', 'newspaper.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    19 , 'Job Announcement', 'job announcement description', 'newspaper.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    19 , 'Television and Ratio', 'Television and Ratio description', 'newspaper.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    19 , 'Megazine and Newspaper', 'Megazine and Newspaper description', 'newspaper.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    19 , 'Technology news', 'Technology news description', 'newspaper.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    19 , 'Economic news', 'Economic news description', 'newspaper.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    19 , 'General news', 'General news description', 'newspaper.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    19 , 'Heath News', 'Megazine and Newspaper description', 'newspaper.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    NULL , 'Government', 'Government description', 'newspaper.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    29 , 'Ministry', 'Ministry description', 'newspaper.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    29 , 'Embassy', 'Embassy description', 'newspaper.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    NULL , 'Accommodation', 'Accommodation', 'hotel.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    32 , 'Hotel', 'Hotel Description', 'hotel.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    32 , 'Guesthouse', 'Guesthouse Description', 'hotel.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    32 , 'Apartment', 'Apartment Description', 'hotel.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    NULL , 'Business Consultant', 'Business consultant description', 'hotel.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    36 , 'Business ', 'Business Description', 'hotel.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    36 , 'Commercial', 'Commercial description', 'hotel.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    NULL , 'Travel and tour', 'Travel and tour description', 'hotel.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    39 , 'Guide Agency', 'Guide agency', 'hotel.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    39 , 'Travel agency', 'Travel agency', 'hotel.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    39 , 'Tour agency', 'Tour agency description', 'hotel.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    NULL , 'Microfinance and Bank', 'Microfinance and Bank description', 'hotel.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    43 , 'Microfinance', 'Microfinance description', 'hotel.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    43 , 'insurance', 'insurance description', 'hotel.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    43 , 'Pawn shop', 'pawn shop description', 'hotel.png'
);
INSERT INTO kd_category(main_cate_id, cate_name, description, icon_name) VALUES(
    43 , 'Bank', 'Bank description', 'hotel.png'
);


-- INSERT INTO kd_cate_keyword(cate_id, keyword_name) VALUES(1, 'edu');


-- insert some test urls

-- INSERT INTO kd_url(title, cate_id, url_type, user_id, link, address,
-- 									 contact, description, pic_url, status, is_approved)
-- 		VALUES (
-- 			'facebook', 4, 'f', 1, 'http://www.facebook.com', 'address of the web site',
-- 			'078 566 967, 090 456 554', 'description of the website', 'pic_url.png', TRUE, TRUE
-- 		);



-- Insert some URL keywords

-- INSERT INTO kd_keyword(url_id, keyword_name, status) VALUES (1, 'facebook', TRUE);




-- Searching Script

-- SELECT id, title, url_type, link, address, contact, description, pic_url FROM kd_url WHERE id IN
-- (
--   SELECT DISTINCT id FROM (
--     (SELECT id FROM kd_url WHERE id IN (SELECT url_id FROM kd_keyword WHERE keyword_name ILIKE 'f%') ORDER BY view)
--       UNION
--     (SELECT id FROM kd_url WHERE cate_id IN (SELECT cate_id FROM kd_cate_keyword WHERE keyword_name ILIKE 'f%') ORDER BY view)
--   ) AS url_id
-- )