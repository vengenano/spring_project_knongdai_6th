package com.kshrd.knongdai.controller.rest;

import com.kshrd.knongdai.response.Response;
import com.kshrd.knongdai.response.ResponseList;
import com.kshrd.knongdai.response.ResponseRecord;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.List;

/**********************************************************************
 *
 * Author: Theara
 * Created Date: 22/July/2017
 * Development Group: PP_G3
 * Description: KnongDaiRestController is a based controller
 *              that provide common response for RESTFul Web Service
 *
 **********************************************************************/

public class KnongDaiRestController<T> {

    public ResponseEntity<Response> successResponse(List<T> data, String msg){
        return new ResponseEntity<Response>(new ResponseList<T>("400", true, msg, data), HttpStatus.OK);
    }

    public ResponseEntity<Response> successResponse(T data, String msg){
        return new ResponseEntity<Response>(new ResponseRecord<T>("200", true, msg, data), HttpStatus.OK);
    }

    public ResponseEntity<Response> successResponse(String msg){
        return new ResponseEntity<Response>(new Response<T>("200", true, msg), HttpStatus.OK);
    }

    public ResponseEntity<Response> failResponse(String msg){
        return new ResponseEntity<Response>(new Response<T>("400", false, msg), HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<Response> internalErrorResponse(){
        return new ResponseEntity<Response>(new Response<T>("500", false, "something went wrong!"), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
