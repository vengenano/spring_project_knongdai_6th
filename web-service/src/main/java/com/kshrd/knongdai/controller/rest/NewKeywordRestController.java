package com.kshrd.knongdai.controller.rest;

import com.kshrd.knongdai.domain.ListNewKeyword;
import com.kshrd.knongdai.domain.ListNewKeywordWithTotalRecord;
import com.kshrd.knongdai.domain.form.NewKeywordCategoryMap;
import com.kshrd.knongdai.domain.form.NewKeywordUrlMap;
import com.kshrd.knongdai.response.Pagination;
import com.kshrd.knongdai.response.PaginationResponseList;
import com.kshrd.knongdai.response.Response;
import com.kshrd.knongdai.service.NewKeywordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**********************************************************************
 *
 * Author: Theara
 * Created Date: 22/July/2017
 * Development Group: PP_G3
 * Description: CategoryRestController is controller for handling RESTFul API
 *              for manipulating Category
 *
 **********************************************************************/

@RestController
@RequestMapping("/api/v1/new-keywords")
public class NewKeywordRestController extends KnongDaiRestController {

    private NewKeywordService newKeywordService;

    @Autowired
    public NewKeywordRestController(NewKeywordService newKeywordService){
        this.newKeywordService = newKeywordService;
    }

    @GetMapping()
    public ResponseEntity<PaginationResponseList<ListNewKeyword>> getKeywordsByPage(
            @RequestParam(value = "sort_by", required = false, defaultValue = "total_traffic") String sortBy,
            @RequestParam(value = "order", required = false, defaultValue = "DESC") String order,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "mapped", required = false) Boolean mapped
    ){
        try{
            sortBy = sortBy.toLowerCase();
            order = order.toUpperCase();
            if(!sortBy.equals("total_traffic") && !sortBy.equals("total_result"))
                sortBy = "total_traffic";
            if(!order.equals("ASC") && !order.equals("DESC"))
                order = "DESC";
            List<ListNewKeywordWithTotalRecord> keywords = newKeywordService.getKeywordsByPage(sortBy, order, mapped, 20, page);
            if(keywords != null){
                Pagination pagination = new Pagination();
                pagination.setTotalRecord(keywords.get(0).getTotalRecord());//Getting total result
                pagination.setCurrentPage(page);
                pagination.setLimit(20);
                PaginationResponseList paginationResponseList = new PaginationResponseList("400", true, "success retreive keywords", keywords);
                paginationResponseList.setPagination(pagination);
                return new ResponseEntity<PaginationResponseList<ListNewKeyword>>(paginationResponseList, HttpStatus.OK);
            }else{
                return failResponse("failed to query result");
            }
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @PostMapping("map-category")
    public ResponseEntity<Response> mapKeywordToCategory(@RequestBody NewKeywordCategoryMap newKeywordCategoryMap){
        try {
            if(newKeywordService.mapKeywordToCategory(newKeywordCategoryMap)){
                return successResponse("successfully mapped the Keyword to the Category");
            }
            return failResponse("failed to mapped the Keyword to the Category!");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @PostMapping("map-url")
    public ResponseEntity<Response> mapKeywordToUrl(@RequestBody NewKeywordUrlMap newKeywordUrlMap){
        try {
            if(newKeywordService.mapKeywordToUrl(newKeywordUrlMap)){
                return successResponse("successfully mapped the Keyword to the URL");
            }
            return failResponse("failed to mapped the Keyword to the URL!");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

}