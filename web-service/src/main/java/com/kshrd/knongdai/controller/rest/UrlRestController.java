package com.kshrd.knongdai.controller.rest;

import com.kshrd.knongdai.domain.FileUploader;
import com.kshrd.knongdai.domain.ListUrl;
import com.kshrd.knongdai.domain.Url;
import com.kshrd.knongdai.response.Pagination;
import com.kshrd.knongdai.response.PaginationResponseList;
import com.kshrd.knongdai.response.Response;
import com.kshrd.knongdai.service.UrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.kshrd.knongdai.configuration.ApplicationResource;

import java.util.List;

/**********************************************************************
 *
 * Author: Sokheang
 * Created Date: 25/July/2017
 * Development Group: PP_G3
 * Description: UrlRestController is controller for handling RESTFul API
 *              for manipulating Url
 *
 **********************************************************************/

@RestController
@RequestMapping("/api/v1/urls")
public class UrlRestController extends KnongDaiRestController {

    private UrlService urlService;
    private FileUploader fileUploader;
    private ApplicationResource applicationResource;

    @Autowired
    public UrlRestController(UrlService urlService, FileUploader fileUploader, ApplicationResource applicationResource){
        this.urlService = urlService;
        this.fileUploader = fileUploader;
        this.applicationResource = applicationResource;
    }

    @PostMapping(value = "/create", headers = "Accept=application/json")
    @Transactional
    public ResponseEntity<Response> createUrl(@RequestBody Url url){
        try {
            if(urlService.insertUrl(url)){
                return successResponse(urlService.getUrlById(url.getId()), "URL has been created successfully");
            }
            return failResponse("failed to create URL");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<Response> getUrlById(@PathVariable("id") int id){
        try {
            Url url = urlService.getUrlById(id);
            if(url != null){
                return successResponse(url, "get URL successfully");
            }
            return failResponse("failed to get URL");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @GetMapping()
    public ResponseEntity<PaginationResponseList<ListUrl>> getUrlsByPage(
            @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
            @RequestParam(value = "title", required = false) String title,
            @RequestParam(value = "main_cate_id", required = false) Integer mainCateId,
            @RequestParam(value = "sub_cate_id", required = false) Integer subCateId,
            @RequestParam(value = "approved", required = false) Boolean approved
    ){
        try {
            int numRow = 30;

            List<ListUrl> urls = urlService.getUrlsByPage(numRow, page, title, mainCateId, subCateId, approved);

            Pagination pagination = new Pagination();
            pagination.setTotalRecord( (urls.size() == 0) ? 0 : urls.get(0).getTotalRecord() );
            pagination.setCurrentPage(page);
            pagination.setLimit(numRow);

            PaginationResponseList paginationResponseList = new PaginationResponseList("400", true, "success retreive URls", urls);

            paginationResponseList.setPagination(pagination);

            return new ResponseEntity<PaginationResponseList<ListUrl>>(paginationResponseList, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @PutMapping(value = "/update", headers = "Accept=application/json")
    @Transactional
    public ResponseEntity<Response> updateSubCategory(@RequestBody Url url){
        try {
            if(urlService.updateUrl(url)){
                return successResponse(urlService.getUrlById(url.getId()), "successfully update URL");
            }
            return failResponse("failed to update URL");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @DeleteMapping("{id}/delete")
    public ResponseEntity<Response> deleteUrlById(@PathVariable("id") int id){
        try {
            if(urlService.deleteUrlById(id)){
                return successResponse("sucessfully delete the URL");
            }
            return failResponse("failed to delete the URL");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @PutMapping("{id}/approved/{approved}")
    public ResponseEntity<Response> updateApproved(@PathVariable("id") int id, @PathVariable("approved") boolean approved){
        try {
            if(urlService.updateApproved(id, approved)){
                return successResponse(((approved) ? "approved" : "disapproved"));
            }
            return failResponse("failed to update the URL's approval");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @GetMapping("keyword-suggestion/{keyword}")
    public List<String> getUrlSuggestedKeywords(@PathVariable("keyword") String keyword){
        return urlService.getUrlSuggestedKeywords(keyword);
    }

    @PostMapping("/upload/web-icon")
    public ResponseEntity<Response> uploadImage(@RequestParam("file") MultipartFile file){
        String uploadPath = applicationResource.getUploadPath();
        String resourcePath = applicationResource.getResourcePath();

        try {

            fileUploader.setResourcePath(uploadPath);
            fileUploader.setResourcesHandler(resourcePath);
            fileUploader.setMultipartFile(file);

            if(fileUploader.upload(null)){
                return successResponse(fileUploader.getAbsolutePath(), "successfully uploaded image");
            }

            return failResponse("failed to upload image");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

}
