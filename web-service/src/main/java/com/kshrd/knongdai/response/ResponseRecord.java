package com.kshrd.knongdai.response;

public class ResponseRecord<T> extends Response<T> {

    private T data;

    public ResponseRecord(String code, boolean status, String msg, T data) {
        super(code, status, msg);
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
