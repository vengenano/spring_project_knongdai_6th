package com.kshrd.knongdai.controller;

import com.kshrd.knongdai.service.CategoryService;
import com.kshrd.knongdai.service.NewKeywordService;
import com.kshrd.knongdai.service.UrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**********************************************************************
 *
 * Author: Sokheang
 * Created Date: 23/July/2017
 * Development Group: PP_G3
 * Description: AdminController is a controller for handling Admin pages
 *
 **********************************************************************/

@Controller
@RequestMapping("/admin")
public class AdminController {

    private CategoryService categoryService;
    private UrlService urlService;
    private NewKeywordService newKeywordService;
    @Autowired
    public AdminController(CategoryService categoryService, UrlService urlService, NewKeywordService newKeywordService){
        this.categoryService = categoryService;
        this.urlService = urlService;
        this.newKeywordService = newKeywordService;
    }

    /**
     *
     * @return to view dashboard
     */
    @RequestMapping("/dashboard")
    public String dashboard(Model model){
        model.addAttribute("TOTALURL",urlService.countActiveUrl());
        model.addAttribute("TOTALSEARCH",newKeywordService.countTotalSearchToday());
        model.addAttribute("TOTALMAIN",categoryService.countTotalMainCate());
        model.addAttribute("TOTALSUB",categoryService.countTotalSubCate());
        return "/admin/dashboard";
    }

    /**
     *
     * @return to view main-category
     */
    @RequestMapping("/main-category")
    public String mainCategory(){
        return "/admin/main-category";
    }

    /**
     *
     * @return to view sub-category
     */
    @RequestMapping("/sub-category")
    public String subCategory(){
        return "/admin/sub-category";
    }

    /**
     *
     * @return to view urls
     */
    @RequestMapping("/urls")
    public String urls(){
        return "/admin/urls";
    }

    @RequestMapping("/keywords")
    public String keyword(){
        return "/admin/keyword";
    }

    @RequestMapping("/scrap")
    public String scrap(Model model){
        model.addAttribute("MAIN_CATES", categoryService.getAllMainCategories());
        return "/admin/scrap";
    }
}
