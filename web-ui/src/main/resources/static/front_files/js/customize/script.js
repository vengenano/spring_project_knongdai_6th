$(document).ready(function () {
    var keyword={};
    var port = 'http://localhost:9090';

    $('.ui.dropdown').dropdown();


    var main = ($("#main-accord").width()+5)+'px';
    var logo = $("#div-img-logo");
    logo.css({
        'width':main
    });
    // $(".mainex").mouseenter(function (e) {
    //     if($(this).find(".div_sub_cate").is(":visible")){
    //         // $(this).find(".div_sub_cate").slideUp(0);
    //         // $(".div_sub_cate").slideUp(0);
    //     }else{
    //         // $(".div_sub_cate").not($(this).find(".div_sub_cate")).hide();
    //         $(this).find(".div_sub_cate").slideDown(200);
    //     }
    //
    //     $(this).find(".div_sub_cate").css('width', $(this).width() + 'px');
    //     $(this).find(".div_sub_cate_scroll").slimScroll({
    //         scrollTo: '0px',
    //         size:'3px',
    //         height:'200px'
    //     });
    // // alert($(this).index(".mainex"));
    //     if($(this).index(".mainex")>=12){
    //
    //     }
    // });
    $(".mainex").click(function (e) {
        if($(this).hasClass("sub-cate-remove")){
            $(this).find(".div_sub_cate").slideUp(200);
            $(this).removeClass("sub-cate-remove");
            $(this).find('.ex2>i').removeClass();
            $(this).find('.ex2>i').addClass('caret down icon iconDrop');
        }else {
            $(".mainex.sub-cate-remove").find(".div_sub_cate").slideUp(200);
            $(".mainex.sub-cate-remove").removeClass("sub-cate-remove");
            $(this).addClass("sub-cate-remove");
            $(this).find(".div_sub_cate").slideDown(200);
            $(this).find('.ex2>i').removeClass();
            $(this).find('.ex2>i').addClass('caret up icon iconDrop');
        }
        $(this).find(".div_sub_cate").css('width', $(this).width() + 'px');
        $(this).find(".div_sub_cate_scroll").slimScroll({
            scrollTo: '0px',
            size:'3px',
            height:'120px'
        });
    });

    var divtxtSearch=$("#div-txt-Search");
    $("#txtSearch").focus(function () {
        $(this).select();
        $("#overlay-div").fadeIn(200);
        $('body').css('overflow-y','hidden');
        window.scrollTo(0, 0);
        document.body.scrollTop = 0;
        $("#eac-container-txtSearch").css("width",divtxtSearch.width() + 'px');
    });
    $('.myleft.lnk-img, .url-title>a, .url-lnk a').click(function (e) {
        e.preventDefault();
        window.location.href = kd.host + '/redirect/'+ $(this).closest('.url-list').data('url-id');
    });
    $("#overlay-div").click(function () {
        $("#overlay-div").fadeOut(300);
        $('body').css('overflow-y','auto');
    });
    keyword.findkeyword = function(data){
        $.ajax({
            url:  port+"/index/search/"+ data,
            type: "GET",
            beforeSend: function(xhr) {
                //$("#search-box").css("background","#FFF url(/resources/front_files/LoaderIcon.gif) no-repeat 98%");
            },
            success: function(data) {
                JSON.stringify(data);
                availableTags.data = data;
                console.log(availableTags);
                // $("#txtSearch").easyAutocomplete(availableTags);
                $("#eac-container-txtSearch").css('width', divtxtSearch.width() + 'px');
            },
            error:function(data,status,er) {
                console.log(data);
            }
        });

    };

    function fixDiv() {
        var $cache = $('#accord-category');
        var size_main = $("#main-accord").width() + 'px';
        var btnScrollTop=$("#btn-scroll-top");

        if (navigator.onLine) {
            if ($(window).scrollTop() > 100){
                // $("#textbox-fixed").removeClass("displayNone");
                $("#textbox-fixed").slideDown(100);
            }else{
                // $("#textbox-fixed").addClass("displayNone");
                $("#textbox-fixed").slideUp(100);
            }
            if($(window).scrollTop() > 200){
                btnScrollTop.css({
                    'opacity':'1'
                })
            }else{
                btnScrollTop.css({
                    'opacity':'0'
                })
            }
            $("#no-connectivity").slideUp(200);
        }else{
            $("#no-connectivity").slideDown(200);
        }
    }
    $(window).scroll(function () {
        fixDiv();
    });
    fixDiv();
    //ScrollTop btn
    $("#btn-scroll-top").click(function () {
        $("body").stop().animate({scrollTop:0}, 500, 'swing', function() {
            // alert("Finished animating");
        });
    });
    $(document).on("click","#txtSearchFixed,#btnSearchFixed,#sub-logo",function () {
        var body = $("html, body");
        body.stop().animate({scrollTop:0}, 200, 'swing', function() {
            $("#txtSearch").focus().select();
        });
    });
    $(document).on("focus","#txtSearchFixed,#btnSearchFixed",function () {
        var body = $("html, body");
        body.stop().animate({scrollTop:0}, 200, 'swing', function() {
            $("#txtSearch").focus().select();
        });
    });
    $("#div-btn-more").click(function () {
        $("#div-for-mobile").slideToggle(200);
    });
    //resize boxes
    $(window).resize(function () {
        if ((window.fullScreen) || (window.innerWidth == screen.width && window.innerHeight == screen.height)) {
            $("#eac-container-txtSearch").css('width', $("#div-txt-Search").width() + 'px');
            $(".div_sub_cate").css('width', $(".mainex").width() + 'px');
        } else {
            $("#eac-container-txtSearch").css('width', $("#div-txt-Search").width() + 'px');
            $(".div_sub_cate").css('width', $(".mainex").width() + 'px');
        }
        var main = ($("#main-accord").width()+5)+'px';
        var logo = $("#div-img-logo");
        $("#div-for-mobile").slideUp(200);
        logo.css({
            'width':main
        });
    });



    $(document).on("click", "#mobile-add-manual", function () {
        $("#div-add-manual").trigger("click");
    });
    $(document).on("click", "#trigger-push-submit-btnAdd", function () {
        $("#btn-submit").trigger("click");
    });
    $(document).on("mouseover", "#ul-Search-dropdown>li", function () {
        $("#ul-Search-dropdown>li").removeClass("selected");
    });
    $('.ui.accordion').accordion();
    $("#txtSearch").css({
        "height":$("#div_btnSearch").outerHeight(true)
    });
});
