package com.kshrd.knongdai.scrape;

import com.kshrd.knongdai.domain.Url;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by phen on 27/07/17.
 */
public class ScrapeAtoZpage {
    public static List<Url> extractData(String mainUrl, String url){
        List<Url> myurl = new ArrayList<>();
        int i =0;
        try{
            Document document = JsoupConnection.getConnection(url).get();
            //System.out.println(document);
            Element tabContent = document.getElementById("tab1");
            //System.out.println(tabContent);
            Elements nameMedia = tabContent.getElementsByClass("name");
            //System.out.println(nameMedia);
            Elements mediaBodys = nameMedia.select("div.media-body");

            //System.out.println(mediaBodys); example it have 10 elements
            for(Element mediaBody : mediaBodys){//we will use for loop to scan these 10 elements
                String suburl = mainUrl + mediaBody.select("p").get(3).select("p>a").get(1).attr("href");
                myurl.add(exstractDetail(suburl));
                i = i++;
                System.out.println(i);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return myurl;
    }

    //If we need jum to de detail page
    public static Url exstractDetail(String suburl){

        Url geturl = new Url();

        String title ="";
        String link="";
        String phone="";
        String email="";
        String address="";
        String description="";
        String pic_url = "";
//        String pic_witch_domain = "";
        try {
//            String url = "http://atozcambodia.com/" + suburl;

            Document document = JsoupConnection.getConnection(suburl).get();

            Elements descrip = document.getElementsByClass("descrip").select("div.row").select("div.col-md-4");
            Elements image = document.getElementsByClass("descrip").select("div.row").select("div.col-md-4").select("img[src~=(?i)\\.(png|jpe?g|gif)]");
            //GET IMAGE
            for(Element img : image){
                pic_url = img.attr("src");
                if(!pic_url.contains("https://")){
                    pic_url = "http://atozcambodia.com/" + pic_url;
                }
            }

            title = descrip.get(0).select("h4").get(0).text();
            phone = descrip.get(1).select("p").get(0).text();
            link = descrip.get(1).select("p").get(1).select("p>a").attr("href");
            if(!link.startsWith("http")){
                link = "http://" + link;
            }
            email = descrip.get(1).select("p").get(2).text();
            address = descrip.get(1).select("p").get(3).text();
            description = descrip.get(2).select("p").get(0).text();

            Url url = new Url();
            url.setTitle(title);
            url.setLink(link);
            url.setUrlType("w");
            url.setPhone(phone);
            url.setEmail(email);
            url.setAddress(address);
            url.setDescription(description);
            url.setMainCateId(1);
            url.setSubCateId(1);
            url.setPicUrl(pic_url);

            geturl = url;

            System.out.println(title);
            System.out.println(link);
            System.out.println(phone);
            System.out.println(email);
            System.out.println(address);
            System.out.println(description);
            System.out.println(pic_url);
            System.out.println("-----------------------------------------------------------");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return geturl;

    }

}
