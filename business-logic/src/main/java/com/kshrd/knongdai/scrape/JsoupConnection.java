package com.kshrd.knongdai.scrape;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

/**
 * Created by phen on 27/07/17.
 */
public class JsoupConnection {

    public static Connection getConnection(String url){
        Connection conn=null;
        try{
            conn= Jsoup.connect(url);
        }catch (Exception e){
            e.printStackTrace();
        }
        return conn;
    }
}
