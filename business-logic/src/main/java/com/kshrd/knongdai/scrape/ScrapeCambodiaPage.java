package com.kshrd.knongdai.scrape;

import com.kshrd.knongdai.domain.Url;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by phen on 03/08/17.
 */
public class ScrapeCambodiaPage {
    public static List<Url> extractData(String mainUrl, String url){

        List<Url> myurl = new ArrayList<>();
//        Url recordData = new Url();
        try{
            System.out.println("START NEXT PAGEs...");
            Document document = JsoupConnection.getConnection(url).timeout(10000).get();
//
            Elements container = document.getElementsByClass("content").select("ol.contain-telephone-number").select("article");
            for(Element box : container){
                Elements info = box.select("section");
                String suburl = mainUrl + info.select("div.listing-info").select("header").get(0).select("h2").get(0).select("a").attr("href");
//              System.out.println(mainUrl+info.select("div.listing-info").select("header").get(0).select("h2").get(0).select("a").attr("href"));
                myurl.add(exstractDetail(suburl));
            }

        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Can not add record to list. ");
        }
        return myurl;
    }



    public static Url exstractDetail(String suburl){

        String setTitle = "";
        String setPhone = "";
        String setEmail = "";
        String setAddress ="";
        String setUrl ="";
        String pic_url = "";
        String setDescription = "";
        Url record = new Url();
        try{
//            System.out.println("Detail page");
            Document document = JsoupConnection.getConnection(suburl).get();

            Elements info = document.getElementsByClass("grid-gutter-height").select("div.span16");

            Elements image = info.select("section").select("div.span6").select("div#listing-logo").select("img");
            pic_url = image.attr("src");
            if(pic_url != ""){
                if(!pic_url.contains("https:")){
                    pic_url = "https:" + pic_url;
                }

            }
//            System.out.println(pic_url);


            Elements TitleElement= info.select("section").select("div.span10").select("div.vcard").select("h3");
            setTitle = TitleElement.text();


            Elements UrlElement = info.select("section").select("div.span10").select("div.vcard").select("ul.primary-communications");
            setUrl = UrlElement.select("li").select("span.text-long").get(0).text();
            if(!setUrl.startsWith("http")){
                setUrl = "http://" + setUrl;
            }

            System.out.println("---------------------------------------------------------");

            //setUrl = UrlElement.attr("href");

            Elements PhoneElement = info.select("section").select("div.span10").select("div.vcard").select("ul.contain-telephone-number").get(0).select("li.health").get(0).select("span");
            setPhone = PhoneElement.get(1).text();


//            Elements EmailElement = info.select("section").select("div.span10").select("div.vcard").select("ul.contain-telephone-number").get(0).select("li").get(3).select("span");
//            setEmail = EmailElement.get(1).text();
//            System.out.println(setEmail);

            Elements AdressElement= info.select("section").select("div.span10").select("div.vcard").select("address").select("span");
            setAddress= AdressElement.text();


            Elements DescriptionElement= info.select("section#catalog").select("ul").select("li").get(0).select("div");
            setDescription= DescriptionElement.after("h5").text();
            if(setDescription.contains("Description")){
                setDescription = setDescription.substring(11);
            }

            if(setUrl.contains("www")){
                if(setUrl != "#"){
                    System.out.println(setTitle);
                    System.out.println(setUrl);
                    System.out.println(setPhone);
                    System.out.println(setAddress);
                    System.out.println(pic_url);
                    System.out.println(setDescription);

                    System.out.println("------------------------------------");
                }

            }

            record = new Url();
            record.setTitle(setTitle);
            record.setLink(setUrl);
            record.setPhone(setPhone);
            record.setAddress(setAddress);
            record.setPicUrl(pic_url);
            record.setDescription(setDescription);


        }catch (Exception e){
            // e.printStackTrace();

        }
        return record;
    }
}
