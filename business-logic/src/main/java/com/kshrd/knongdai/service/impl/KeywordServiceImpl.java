package com.kshrd.knongdai.service.impl;

import com.kshrd.knongdai.persistence.KeywordRepository;
import com.kshrd.knongdai.service.KeywordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Mr.Pheng on 7/26/2017.
 */
@Service
public class KeywordServiceImpl implements KeywordService {

    private KeywordRepository keywordRepository;

    @Autowired
    public KeywordServiceImpl (KeywordRepository keywordRepository){
        this.keywordRepository=keywordRepository;
    }

    @Override
    public List<String> getkeywordByName(String name) {
        return keywordRepository.getkeyword(name);
    }
}
