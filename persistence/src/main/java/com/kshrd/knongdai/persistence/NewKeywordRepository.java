package com.kshrd.knongdai.persistence;

import com.kshrd.knongdai.domain.ListNewKeywordWithTotalRecord;
import com.kshrd.knongdai.domain.NewKeyword;
import com.kshrd.knongdai.domain.form.NewKeywordCategoryMap;
import com.kshrd.knongdai.domain.form.NewKeywordUrlMap;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**********************************************************************
 *
 * Author: Theara
 * Created Date: 2/Aug/2017
 * Development Group: PP_G3
 * Description: NewKeywordRepository interface is used as DAO
 *              for manipulating NewKeyword
 *
 **********************************************************************/

@Repository
public interface NewKeywordRepository {

    @Insert("INSERT INTO kd_new_keyword (keyword_name, total_result) " +
            "VALUES (#{keyword.keywordName}, #{keyword.totalResult})")
    boolean insertNewKeyword(@Param("keyword") NewKeyword keyword);

    @Delete("DELETE FROM kd_new_keyword WHERE id = #{id}")
    boolean deleteKeyword(@Param("id") int id);

    @Select("<script>" +
            "SELECT NK.keyword_name, COUNT(*) AS total_traffic, MAX(NK.total_result) AS total_result,\n" +
            "  EXISTS (SELECT 1 FROM kd_cate_keyword CK WHERE CK.keyword_name = NK.keyword_name\n" +
            "    UNION\n" +
            "    SELECT 1 FROM kd_keyword UK WHERE UK.keyword_name = NK.keyword_name\n" +
            "    UNION\n" +
            "    SELECT 1 FROM kd_url U WHERE U.title = NK.keyword_name\n" +
            "  ) AS mapped,\n" +
            "  COUNT(*) OVER() AS total_record\n" +
            "FROM kd_new_keyword NK\n" +
            "WHERE NK.status = TRUE\n" +
            "<if test=\"mapped != null\">" +
            " AND EXISTS (SELECT 1 FROM kd_cate_keyword CK WHERE CK.keyword_name = NK.keyword_name\n" +
            "    UNION\n" +
            "    SELECT 1 FROM kd_keyword UK WHERE UK.keyword_name = NK.keyword_name\n" +
            "    UNION\n" +
            "    SELECT 1 FROM kd_url U WHERE U.title = NK.keyword_name\n" +
            "  ) = " +
            "<if test=\"mapped == true\">TRUE</if>" +
            "<if test=\"mapped == false\">FALSE</if>" +
            "</if>\n" +
            "GROUP BY NK.keyword_name\n" +
            "<if test=\"sort_by == 'total_traffic'\">ORDER BY total_traffic</if>\n" +
            "<if test=\"sort_by == 'total_result'\">ORDER BY total_result</if>\n" +
            "<if test=\"order == 'DESC'\">DESC</if>\n" +
            "<if test=\"order == 'ASC'\">ASC</if>\n" +
            "LIMIT #{num_row} OFFSET (#{num_row} * (#{page} - 1))" +
            "</script>")
    @Results(value = {
            @Result(property = "keywordName", column = "keyword_name"),
            @Result(property = "totalTraffic", column = "total_traffic"),
            @Result(property = "totalResult", column = "total_result"),
            @Result(property = "totalRecord", column = "total_record")
    })
    List<ListNewKeywordWithTotalRecord> getKeywordsByPage(
            @Param("sort_by") String sortBy,
            @Param("order") String order,
            @Param("mapped") Boolean mapped,
            @Param("num_row") int numRow,
            @Param("page") int page
            );

    @Insert("INSERT INTO kd_cate_keyword (cate_id, keyword_name) VALUES (#{obj.categoryId}, #{obj.keyword})")
    boolean mapKeywordToCategory(@Param("obj") NewKeywordCategoryMap newKeywordCategoryMap);

    @Insert("INSERT INTO kd_keyword (url_id, keyword_name) VALUES (#{obj.urlId}, #{obj.keyword})")
    boolean mapKeywordToUrl(@Param("obj") NewKeywordUrlMap newKeywordUrlMap);

    @Select("SELECT COUNT(*) FROM kd_new_keyword WHERE status = TRUE")
    int countTotalSearch();

    @Select("SELECT COUNT(*) FROM kd_new_keyword WHERE DATE(created_date) = CURRENT_DATE")
    int countTotalSearchToday();
}
