package com.kshrd.knongdai.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**********************************************************************
 *
 * Author: Sokheang
 * Created Date: 3/Aug/2017
 * Development Group: PP_G3
 * Description: ResultUrl class is used to create Url response to
 *              to the client side.
 *
 **********************************************************************/

public class ResultUrl {

    private int id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("type")
    private String urlType;
    private String link;
    private String address;
    private String phone;
    private String email;
    @JsonProperty("des")
    private String description;
    @JsonProperty("pic_url")
    private String picUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title.trim();
    }

    public String getUrlType() {
        return urlType;
    }

    public void setUrlType(String urlType) {
        this.urlType = urlType.trim();
    }

    public String getLink() {
        return concatenateHttp(link);
    }

    public void setLink(String link) {
        this.link = concatenateHttp(link.trim());
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = setNullIfStringEmpty(address.trim());
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = setNullIfStringEmpty(phone.trim());
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = setNullIfStringEmpty(email.trim());
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = setNullIfStringEmpty(description.trim());
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = setNullIfStringEmpty(picUrl.trim());
    }

    private String concatenateHttp(String link){
        return (link.startsWith("http")) ? link : "http://" + link;
    }

    private String setNullIfStringEmpty(String str){
        return (str.trim().isEmpty()) ? null : str;
    }

}
