package com.kshrd.knongdai.domain.form;

import com.fasterxml.jackson.annotation.JsonProperty;

/**********************************************************************
 *
 * Author: Phen
 * Created Date: 26/July/2017
 * Development Group: PP_G3
 * Description: UserAddForm class is used for adding User operation.
 *
 **********************************************************************/

public class UserAddForm {

    private int id;
    @JsonProperty("user_name")
    private String userName;
    private String email;
    @JsonProperty("first_password")
    private String firstPassword;
    @JsonProperty("second_password")
    private String secondPassword;
    @JsonProperty("role_id")
    private int roleId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstPassword() {
        return firstPassword;
    }

    public void setFirstPassword(String firstPassword) {
        this.firstPassword = firstPassword;
    }

    public String getSecondPassword() {
        return secondPassword;
    }

    public void setSecondPassword(String secondPassword) {
        this.secondPassword = secondPassword;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }
}
