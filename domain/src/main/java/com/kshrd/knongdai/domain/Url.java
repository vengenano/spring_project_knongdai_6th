package com.kshrd.knongdai.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**********************************************************************
 *
 * Author: Theara
 * Created Date: 3/Aug/2017
 * Development Group: PP_G3
 * Description: Url is an extension of ResultUrl with additional two
 *              additional field, mainCateId and subCateId.
 *              It is used for add and update operation.
 *
 **********************************************************************/

import java.util.List;

public class Url extends ResultUrl {

    @JsonProperty("main_cate_id")
    private int mainCateId;
    @JsonProperty("sub_cate_id")
    private int subCateId;
    @JsonIgnore
    @JsonProperty("user_id")
    private Integer userId;
    private boolean status;
    @JsonProperty("approved")
    private boolean approved;
    private int view;
    private List<String> keywords;

    public int getMainCateId() {
        return mainCateId;
    }

    public void setMainCateId(int mainCateId) {
        this.mainCateId = mainCateId;
    }

    public int getSubCateId() {
        return subCateId;
    }

    public void setSubCateId(int subCateId) {
        this.subCateId = subCateId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public int getView() {
        return view;
    }

    @JsonIgnore
    public void setView(int view) {
        this.view = view;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }
}
