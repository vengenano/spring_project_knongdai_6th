package com.kshrd.knongdai.domain.form;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kshrd.knongdai.domain.BaseCategory;

import java.util.List;

/**********************************************************************
 *
 * Author: Vannak
 * Created Date: 24/July/2017
 * Development Group: PP_G3
 * Description: CategoryAddForm class is used for Category adding operation
 *
 **********************************************************************/
public class CategoryAddForm extends BaseCategory {

    private List<String> keywords;

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

}
