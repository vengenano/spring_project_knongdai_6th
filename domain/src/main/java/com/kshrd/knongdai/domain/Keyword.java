package com.kshrd.knongdai.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**********************************************************************
 *
 * Author: Chanpheng
 * Created Date: 24/July/2017
 * Development Group: PP_G3
 * Description: Keyword class is used as a keyword field of Category Model
 *
 **********************************************************************/

public class Keyword extends BaseEntity {

    @JsonProperty("keyword_name")
    private String keywordName;
    @JsonProperty("cate_id")
    private String cateID;
    public String getKeywordName() {
        return keywordName;
    }

    public void setKeywordName(String keywordName) {
        this.keywordName = keywordName.trim();
    }

    public String getCateID() {
        return cateID;
    }

    public void setCateID(String cateID) {
        this.cateID = cateID;
    }
}
